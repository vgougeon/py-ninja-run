# py-ninja-run

Bonjour !
Voici mon projet python pour Ynov
C'est un excellent jeu, quoique un peu nul

**Librairies utilisées**

    1 - pygame (seulement les fonctions pour afficher des formes et des images)
    
    2 - sqlite3, pour stocker en mémoire les données concernant les sprites, les animations, les stats...

**Installation**

Pour lancer le jeu il faut executer main.py

Il faudra aussi installer pygame

            pip install pygame

**Le jeu**

Mon jeu est un side-scroller : le but est d'aller le plus loin possible sans mourir. Plus on est loin, plus les monstres sont puissants.

Ce jeu n'a rien pour lui : il n'est pas amusant, pas original et surtout il n'a aucun intérêt. Et pour couronner le tout il demande beaucoup trop de performances.

**Les touches**

    Flèche gauche : Aller à gauche 
    Flèche droite : Aller à droite
    E : donner un coup d'épée puissant  (cooldown 4000ms) base damage 120
    F : donner un coup d'épée non puissant  (cooldown 500ms) base damage 30

**Les fichiers**

    ninjarun
        Base de données Sqlite
    main.py
        Créé la fenêtre et appelle la fonction game.loop de la classe Game
    game.py
        Contient le boucle principal avec mise de camera, interface, entities...
    sprite.py
        Contient les images des entités (monstres et player), les animations et les barres de vie
    spell.py
        Classe designant une attaque d'un sprite, contient un cooldown, des degats, etc...
    SoundManager.py
        Joue des sons au hasard dans un dossier donné
    player.py
        Classe du joueur : contient toutes ses stats et actualise la position en fonction d'une acceleration.
    monster.py
        Stats d'un monstre et acceleration
    EntityManager.py
        Contient tous les monstres
    background.py
        Contient l'url du background et tous les calque de celui ci
    layer.py
        Contient un calque du background.py
    keyboard.py
        Utilisé pour savoir si une touche est activement pressée
    interface.py
        Barre de vie et d'xp, niveau et image du joueur
    FloatingText.py
        Affichage d'un texte flottant qui disparait en 1-2sec, utilisé pour les dégats et coups critiques
    camera.py
        Pour suivre le joueur et positionner le reste en fonction de cette caméra

Les assets viennent de itch.io et sont gratuits, sauf la musique que j'ai créé.
