import pygame
from src.game import Game
from src.camera import Camera
def main():
    pygame.init()
    # w, h = 1100, 620
    w, h = 1280, 600
    display = pygame.display.set_mode((w, h))
    cam = Camera(w, h, display)
    pygame.display.set_caption("Ninja Run")
    game = Game(display, cam)
    game.loop()
main()
