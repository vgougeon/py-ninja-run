import pygame
class Menu:
    def __init__(cls, game):
        cls.game = game
        cls.menu = True
        cls.gameover = False
        cls.title = pygame.freetype.Font("assets/interface/damages.ttf", 62)
        cls.font = pygame.freetype.Font("assets/interface/rb.ttf", 15)
        cls.ui = {
            "player": cls.importImage("assets/interface/player2.png", 1),
            "lvl": cls.importImage("assets/interface/lvl.png", 1),
            "play": cls.importImage("assets/interface/play.png", 1),
        }
    def loop(cls):
        cls.menu = True
        cls.game.pause = False
        while cls.menu:
            cls.game.background.draw()
            cls.game.cam.x += 0.2
            if(cls.gameover == False):
                text = cls.title.render("Ninja Run", (255,255,255))[0]
            else:
                text = cls.title.render("Game Over", (255,50,50))[0]

            cls.game.display.blit(text, (cls.game.cam.w / 2 - text.get_width() /2, cls.game.cam.h / 2 - 70))

            cls.game.display.blit(cls.ui["player"], (cls.game.cam.w / 2 - cls.ui["player"].get_size()[0] / 2 , cls.game.cam.h / 2 - 180))
            cls.game.display.blit(cls.ui["lvl"], (cls.game.cam.w / 2 - cls.ui["lvl"].get_size()[0] / 2 , cls.game.cam.h / 2 - 135))
            cls.game.display.blit(cls.ui["play"], (cls.game.cam.w / 2 - cls.ui["play"].get_size()[0] / 2 , cls.game.cam.h / 2 + 50))

            level = cls.font.render(str(cls.game.player.level), (255,255,255))[0]
            cls.game.display.blit(level, (cls.game.cam.w / 2 - level.get_width() /2 - 2, cls.game.cam.h / 2 - 113))

            for event in pygame.event.get():
                if event.type == pygame.QUIT: exit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE: cls.exitMenu()
                if event.type == pygame.MOUSEBUTTONUP:
                    position = pygame.mouse.get_pos()
                    if position[0] < (cls.game.cam.w / 2 + cls.ui["play"].get_size()[0] / 2) and \
                       position[0] > (cls.game.cam.w / 2 - cls.ui["play"].get_size()[0] / 2) and \
                       position[1] < (cls.game.cam.h / 2 + 75 + cls.ui["play"].get_size()[1] / 2) and \
                       position[1] > (cls.game.cam.h / 2 + 75 - cls.ui["play"].get_size()[1] / 2) and \
                       True == True:
                        cls.exitMenu()

            pygame.display.update()

    def importImage(cls, url, scale):
        image = pygame.image.load(url)
        image = pygame.transform.scale(image.convert_alpha(), (image.get_size()[0] * scale , image.get_size()[1] * scale))
        return image

    def exitMenu(cls):
        if(cls.gameover == False):
            cls.menu = False
        else:
            exit()
