import pygame
import sqlite3
from random import randint
from src.keyboard import Keyboard
from src.sprite import Sprite
from src.spell import Spell
class Player:
    def __init__(cls, display, cam, key, game):
        cls.conn = sqlite3.connect('ninjarun')
        cls.conn.row_factory = sqlite3.Row
        cls.c = cls.conn.cursor()
        cls.game = game
        cls.c.execute("SELECT * FROM sprites WHERE name = 'player'")
        cls.row = cls.c.fetchone()

        cls.display = display
        cls.sprite = Sprite(display, cls.row, 3)
        cls.cam = cam
        cls.key = key
        cls.speed = int(cls.row["speed"])
        cls.y = 300
        cls.x = 0
        cls.ax = 0
        cls.vx = 0
        cls.ay = 0
        cls.vy = 0

        #Spells
        spells = cls.row["spells"]
        cls.action = False
        cls.abilities = []
        for spell in cls.c.execute("SELECT * FROM abilities WHERE id IN (" + spells + ")"):
            cls.abilities.append(Spell(spell, cls))
        cls.hp = 100
        cls.maxHp = 100
        cls.bonusDamage = 0
        #Stats
        cls.bestdistance = 200
        cls.xp = 0
        cls.maxXp = 100
        cls.level = 1

        cls.regenRate = 250
        cls.criticalRate = 10
        cls.lastTickRegen = pygame.time.get_ticks()

    def draw(cls):
        cls.update()
        cls.action = False
        if(cls.key.F == True):
            cls.action = cls.ability("attack").use()
        elif(cls.key.E == True):
            cls.action = cls.ability("up_attack").use()

        if(cls.action != False):
            cls.game.sm.playSword()
        cls.sprite.draw(cls.x - cls.cam.x, cls.y, cls.vx, cls.vy, cls.ax, cls.ay, cls.action)
    def update(cls):
        #Y calc
        cls.ay = 1
        if(cls.y >= cls.cam.h - cls.sprite.h + int(cls.row["height"])):
            cls.y = cls.cam.h - cls.sprite.h + int(cls.row["height"])
            cls.vy = 0
            cls.ay = 0
        if(cls.key.space & (cls.vy == 0)):
            cls.ay = -5
            cls.vy = -20
        cls.vy = cls.vy * 0.96 + cls.ay
        cls.y += cls.vy

        #X calc
        friction = 0.7
        if(cls.vy != 0): friction = 0.98
        cls.ax = 0
        if(cls.key.right):
            cls.ax = cls.speed/4
        if(cls.key.left):
            cls.ax = -cls.speed/4
        cls.vx = cls.vx * friction + cls.ax
        if(cls.vx > cls.speed): cls.vx = cls.speed
        if(cls.vx < -cls.speed): cls.vx = -cls.speed
        cls.x += cls.vx

        #Regen HP
        tick = pygame.time.get_ticks()
        if(tick > cls.lastTickRegen + cls.regenRate):
            cls.hp += 1
            if(cls.hp > cls.maxHp):
                cls.hp = cls.maxHp
            cls.lastTickRegen = tick

        #Bonus Distance
        if(cls.x > cls.bestdistance):
            cls.giveExp((cls.x - cls.bestdistance)/30)
            cls.bestdistance = cls.x

        for monster in cls.game.em.entities:
            if(monster.action != False):
                cls.damage(monster.level * 10)


    def giveExp(cls, amount):
        cls.xp += amount
        if(cls.xp >= cls.maxXp):
            cls.game.sm.playLevelUp()
            cls.xp = cls.xp - cls.maxXp
            cls.level += 1
            cls.bonusDamage += 40
            cls.maxHp += 20
            cls.hp = cls.maxHp
            cls.maxXp += 50

    def toArray(cls, arr):
        return arr.split(",")

    def ability(cls, name):
        i = 0
        for spell in cls.abilities:
            if(spell.name == name):
                return cls.abilities[i]
            i += 1

    def damage(cls, amount):
        cls.hp -= amount
        if(cls.hp < 0):
            cls.hp = 0
    def critical(cls):
        r = randint(0, 100)
        if(r < cls.criticalRate):
            return (1 + r*0.01)
        return 0
