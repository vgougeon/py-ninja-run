import pygame
import os
from src.layer import Layer
class Background:
    def __init__(cls, display, cam):
        cls.display = display
        cls.folder = "assets/background/night"
        cls.offset = 0
        cls.cam = cam
        cls.layers = []
        i = 0
        n = len(os.listdir(cls.folder))
        for filename in os.listdir(cls.folder):
            object = pygame.image.load(cls.folder + "/" + filename).convert_alpha()
            object = pygame.transform.scale(object, (cls.cam.w , cls.cam.h))
            layer = Layer(object, cls.cam, cls.display, i)
            cls.layers.append(layer)
            i += 1 / (n - 1)
    def draw(cls):
        for i in range(len(cls.layers)):
            cls.layers[i].draw()
