import pygame

class FloatingText:
    def __init__(cls, display, text, x, y, critical):
        cls.display = display
        cls.text = text
        cls.time = pygame.time.get_ticks()
        cls.font = pygame.freetype.Font("assets/interface/damages.ttf", 32)
        cls.crit = pygame.freetype.Font("assets/interface/damages.ttf", 64)
        cls.x = x
        cls.y = y
        cls.critical = critical
    def draw(cls):
        if(cls.y < 0): return False
        cls.y -= 2
        tick = pygame.time.get_ticks()
        if(tick - 350 > cls.time):
            cls.y -= (tick - cls.time)/15
        if(cls.critical):
            text = cls.crit.render(str(cls.text), (255,50,50))[0]
        else:
            text = cls.font.render(str(cls.text), (255,255,255))[0]
        cls.display.blit(text, (cls.x - text.get_width() /2, cls.y))
        return True
