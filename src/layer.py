import pygame

class Layer:
    def __init__(cls, object, cam, display, slowness):
        cls.object = object
        cls.cam = cam
        cls.display = display
        cls.slowness = slowness
        cls.currentX = 0
    def draw(cls):
        normal = (-cls.cam.x) * cls.slowness
        offset = normal // -cls.cam.w
        cls.display.blit(cls.object, (normal + offset * cls.cam.w, 0))
        cls.display.blit(cls.object, (normal + cls.cam.w + offset * cls.cam.w, 0))































        # cls.display.blit(cls.object, ((-cls.cam.x) * slowness, 0))
        # cls.display.blit(cls.object, ((-cls.cam.x) * cls.slowness + cls.cam.w ,0))
        # cls.display.blit(cls.object, (-cls.cam.x + cls.cam.w ,0))
