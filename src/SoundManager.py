import pygame
import os
from random import randint
class SoundManager:
    def __init__(cls, game):
        cls.game = game
        cls.ost = []
        for filename in os.listdir("assets/sound/ost"):
            cls.ost.append(pygame.mixer.Sound("assets/sound/ost/" + filename))
        cls.sword = []
        for filename in os.listdir("assets/sound/sword"):
            cls.sword.append(pygame.mixer.Sound("assets/sound/sword/" + filename))
        cls.hit = []
        for filename in os.listdir("assets/sound/hit"):
            cls.hit.append(pygame.mixer.Sound("assets/sound/hit/" + filename))
        cls.levelup = []
        for filename in os.listdir("assets/sound/levelup"):
            cls.levelup.append(pygame.mixer.Sound("assets/sound/levelup/" + filename))

        cls.playmusic()
    def playmusic(cls):
        cls.ost[0].play()

    def playSword(cls):
        sound = randint(0, len(cls.sword)-1)
        cls.sword[sound].play()
    def playHit(cls):
        sound = randint(0, len(cls.hit)-1)
        cls.hit[sound].play()
    def playLevelUp(cls):
        sound = randint(0, len(cls.levelup)-1)
        cls.levelup[sound].play()
