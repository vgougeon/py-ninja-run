import pygame
import sqlite3
from src.monster import Monster
from random import randint
class EntityManager:
    def __init__(cls, display, cam, player, game):
        cls.game = game
        cls.display = display
        cls.cam = cam
        cls.player = player
        cls.entities = []
        cls.entities.append(Monster(cls.display, cls.cam, "minotaur", cls.player.x, cls.game))
        cls.entities.append(Monster(cls.display, cls.cam, "light_bandit", cls.player.x, cls.game))

        cls.conn = sqlite3.connect('ninjarun')
        cls.conn.row_factory = sqlite3.Row
        cls.c = cls.conn.cursor()
    def render(cls):
        for i in range(len(cls.entities)):
            if(cls.entities[i].update(cls.player) == False):
                cls.player.giveExp(cls.entities[i].maxHp/5)
                del cls.entities[i]
                break
            cls.entities[i].draw()
        if(len(cls.entities) < 2):
            #entitée aléatoire
            entities = []
            for name in cls.c.execute("SELECT name FROM sprites"):
                entities.append(name["name"])
            newEntity = entities[randint(0,len(entities)-1)]
            cls.entities.append(Monster(cls.display, cls.cam, newEntity, cls.player.x, cls.game))
