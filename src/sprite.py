import pygame
import sqlite3
class Sprite:
    def __init__(cls, display, row, scale):
        cls.display = display
        cls.row = row
        cls.file = pygame.image.load("assets/entities/" + row["path"] + "").convert_alpha()
        cls.xframe = cls.file.get_width() // int(row["w"])
        cls.file = pygame.transform.scale(cls.file, (cls.file.get_width() * scale, cls.file.get_height() * scale))
        cls.lastAnim = "walk"
        cls.anim = "walk"
        cls.forced = False
        cls.margin = 0
        cls.w = int(row["w"]) * scale
        cls.h = int(row["h"]) * scale
        cls.facing = 0
        cls.index = 0
        cls.currFrame = -1
        cls.time = pygame.time.get_ticks()

        cls.font = pygame.freetype.Font("assets/interface/osw.ttf", 14)

        cls.conn = sqlite3.connect('ninjarun')
        cls.conn.row_factory = sqlite3.Row
        cls.c = cls.conn.cursor()

    def draw(cls, x, y, vx, vy, ax, ay, forced = False):
        if(vx < -0.1): cls.facing = 1
        elif(vx > 0.1): cls.facing = 0

        #Quelle animation jouer ?
        if((vx > 0.15) | (vx < -0.15)): cls.anim = 'walk'
        if((vx < 0.15) & (vx > -0.15)): cls.anim = 'idle'
        if(vy != 0): cls.anim = 'jump'

        if((forced != False) & (cls.forced == False)):
            cls.forced = forced
            cls.currFrame = -1
        if(cls.forced != False):
            cls.anim = cls.forced


        cls.c.execute("SELECT * FROM animations WHERE name = '" + cls.anim + "' AND entity = '" + cls.row["name"] + "'")
        row = cls.c.fetchone()
        if(row == None):
            cls.c.execute("SELECT * FROM animations WHERE name = 'idle' AND entity = '" + cls.row["name"] + "'")
            row = cls.c.fetchone()
        frames = cls.toArray(row['frames'])
        tick = pygame.time.get_ticks()
        if(cls.anim != cls.lastAnim):
            cls.currFrame = 0
            cls.index = int(frames[0])
            cls.time = tick

        if(cls.currFrame == -1):
            cls.currFrame = 0
            cls.index = int(frames[0])

        #Animation
        if(tick > cls.time + 110):
            if(cls.currFrame >= len(frames) - 1):
                cls.index = int(frames[cls.currFrame])
                cls.currFrame = 0
                cls.forced = False
            else:
                cls.currFrame += 1
                cls.index = int(frames[cls.currFrame])
            cls.time = tick
        cls.lastAnim = cls.anim
        # n'afficher que le bon sprite du spritesheet
        render = pygame.Surface((cls.w, cls.h), pygame.SRCALPHA)
        render.blit(cls.file,(0, 0), (cls.w * cls.toX(cls.index), cls.h * cls.toY(cls.index), cls.w, cls.h))
        if(cls.facing): render = pygame.transform.flip(render, True, False)
        cls.display.blit(render, (x - render.get_width()/2,y))

    def drawHp(cls, x, y, hp, max):
        width = 75
        height = 8
        bar = pygame.Surface((width,height), pygame.SRCALPHA)
        bar.fill((0,0,0,96))
        health = pygame.Surface((width * (hp / max),height), pygame.SRCALPHA)
        health.fill((203,50,50,255))
        bar.blit(health, (0,0))
        cls.display.blit(bar, (x - width/2,y))
    def drawLvl(cls, x, y, level):
        level = cls.font.render("Level " + str(int(level)), (255,255,255))[0]
        cls.display.blit(level, (x - level.get_width() /2, y))
    def toArray(cls, arr):
        return arr.split(",")

    def toX(cls, index):
        return index % cls.xframe

    def toY(cls, index):
        return index // cls.xframe
