import pygame
import pygame.freetype
class Interface:
    def __init__(cls, display, player):
        cls.display = display
        cls.player = player
        cls.font = pygame.freetype.Font("assets/interface/rb.ttf", 15)
        cls.ui = {
            "player": cls.importImage("assets/interface/player2.png", 1),
            "bar": cls.importImage("assets/interface/bar.png", 2),
            "grid": cls.importImage("assets/interface/grid.png", 2),
            "hp": cls.importImage("assets/interface/hp.png", 2),
            "xp": cls.importImage("assets/interface/xp.png", 2),
            "lvl": cls.importImage("assets/interface/lvl.png", 1)
        }
    def render(cls):
        margin = 90
        margintop = 20

        #HP BAR
        hp = cls.player.hp / cls.player.maxHp
        cls.display.blit(cls.ui["bar"], (margin, margintop))
        cls.display.blit(cls.ui["hp"], (margin, margintop), (0, 0, cls.ui["hp"].get_size()[0] * hp, cls.ui["hp"].get_size()[1]))
        cls.display.blit(cls.ui["grid"], (margin, margintop))

        #XP BAR
        xp = cls.player.xp / cls.player.maxXp
        cls.display.blit(cls.ui["bar"], (margin, margintop + 22))
        cls.display.blit(cls.ui["xp"], (margin, margintop + 22), (0, 0, cls.ui["xp"].get_size()[0] * xp, cls.ui["xp"].get_size()[1]))
        cls.display.blit(cls.ui["grid"], (margin, margintop + 22))

        #LEVEL
        # cls.display.blit(cls.ui["info"], (margin, margintop + 44))

        cls.display.blit(cls.ui["player"], (10, 10))
        cls.display.blit(cls.ui["lvl"], (55, 55))

        level = cls.font.render(str(cls.player.level), (255,255,255))[0]
        cls.display.blit(level, (82 - level.get_width() /2, 77))
        # cls.font.render_to(cls.display, (76, 75), "1", (200, 200, 200))

    def importImage(cls, url, scale):
        image = pygame.image.load(url)
        image = pygame.transform.scale(image.convert_alpha(), (image.get_size()[0] * scale , image.get_size()[1] * scale))
        return image
