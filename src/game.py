import pygame
import sqlite3
from src.keyboard import Keyboard
from src.player import Player
from src.background import Background
from src.camera import Camera
from src.EntityManager import EntityManager
from src.interface import Interface
from src.SoundManager import SoundManager
from src.FloatingText import FloatingText
from src.menu import Menu
class Game:
    def __init__(cls, display, cam):
        cls.display = display
        cls.score = 0
        cls.run = True
        cls.pause = False
        cls.menu = Menu(cls)
        cls.clock = pygame.time.Clock()
        cls.cam = cam
        cls.key = Keyboard()
        cls.player = Player(cls.display, cls.cam, cls.key, cls)
        cls.em = EntityManager(cls.display, cls.cam, cls.player, cls)
        cls.sm = SoundManager(cls)
        cls.background = Background(cls.display, cls.cam)
        cls.text = []
    def loop(cls):
        cls.menu.loop()
        interface = Interface(cls.display, cls.player)
        while cls.run:
            pygame.display.set_caption('Ninja Run : ' + str(cls.clock.get_fps()))
            cls.clock.tick(60)
            if(cls.pause):
                cls.menu.loop()
            if(cls.player.hp <= 0):
                cls.menu.gameover = True
                cls.menu.loop()
            # Actions
            cls.cam.update(cls.player)
            cls.background.draw()
            cls.em.render()
            cls.player.draw()
            interface.render()
            for i in reversed(range(len(cls.text))):
                if(cls.text[i].draw() == False): del cls.text[i]

            # Recuperation des evenements
            cls.key.space = False
            for event in pygame.event.get():
                if event.type == pygame.QUIT: exit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE: cls.pause = True
                    if event.key == pygame.K_SPACE: cls.key.space = True
                    if event.key == pygame.K_RIGHT: cls.key.right = True
                    if event.key == pygame.K_LEFT: cls.key.left = True
                    if event.key == pygame.K_f: cls.key.F = True
                    if event.key == pygame.K_e: cls.key.E = True
                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_SPACE: cls.key.space = False
                    if event.key == pygame.K_RIGHT: cls.key.right = False
                    if event.key == pygame.K_LEFT: cls.key.left = False
                    if event.key == pygame.K_f: cls.key.F = False
                    if event.key == pygame.K_e: cls.key.E = False
            pygame.display.update()
