import pygame
import sqlite3
from src.sprite import Sprite
from src.player import Player
from src.FloatingText import FloatingText
from random import randint
class Monster:
    def __init__(cls, display, cam, type, x, game):
        cls.y = 300
        cls.x = x + randint(cam.w,cam.w*2)
        cls.conn = sqlite3.connect('ninjarun')
        cls.conn.row_factory = sqlite3.Row
        cls.c = cls.conn.cursor()
        cls.action = False
        cls.lastHit = 0
        cls.c.execute("SELECT * FROM sprites WHERE name = '" + type + "'")
        cls.row = cls.c.fetchone()
        cls.knocked = -1
        cls.display = display
        cls.sprite = Sprite(display, cls.row, 3)
        cls.cam = cam
        cls.speed = int(cls.row["speed"])
        cls.ready = -1
        cls.level = cls.x // 800
        cls.ax = 0
        cls.vx = 0
        cls.ay = 0
        cls.vy = 0
        cls.maxHp = int(cls.row["base_hp"]) + (cls.x /25)
        cls.hp = cls.maxHp


        cls.game = game
    def draw(cls):
        if((cls.x > cls.cam.x - cls.cam.w) | (cls.x < cls.cam.x + cls.cam.w)):
            cls.sprite.draw(cls.x - cls.cam.x, cls.y, cls.vx, cls.vy, cls.ax, cls.ay, cls.action)
            cls.sprite.drawHp(cls.x - cls.cam.x, cls.y, cls.hp, cls.maxHp)
            cls.sprite.drawLvl(cls.x - cls.cam.x, cls.y + 15, cls.level)

    def update(cls, player):
        cls.action = False
        # if(abs(cls.x - player.x) > cls.cam.w * 2) | (abs(cls.x - player.x) > cls.cam.w * 1.2) & (player.x > cls.x)):
        if (abs(cls.x - player.x) > cls.cam.w * 2) or \
           (abs(cls.x - player.x) > cls.cam.w * 1.2) and \
           (cls.x < player.x):
            cls.maxHp = 0
            cls.hp = 0
            return False
        #Y calc
        friction = 0.92
        if(cls.y >= cls.cam.h - cls.sprite.h + int(cls.row["height"])):
            cls.y = cls.cam.h - cls.sprite.h + int(cls.row["height"])
            cls.vy = 0
            cls.ay = 0
        cls.vy = cls.vy * 0.96 + cls.ay
        cls.y += cls.vy
        cls.ay = 1
        cls.ax = 0
        if(cls.knocked == -1):
            #Acceleration vers le joueur
            direction = 0
            if(player.x > cls.x + player.sprite.w/2):
                cls.ready = -1
                direction = 1
            elif(player.x < cls.x - player.sprite.w/2):
                cls.ready = -1
                direction = -1
            else:
                if(cls.lastHit < pygame.time.get_ticks() - 1000):
                    if(cls.ready == -1): cls.ready = pygame.time.get_ticks()
                    else:
                        if(pygame.time.get_ticks() - 50 > cls.ready):
                            cls.action = "attack"
                            cls.lastHit = pygame.time.get_ticks()
                            cls.ready = -1
            friction = 0.7
            if(cls.vy != 0): friction = 0.98
            cls.ax = (cls.speed/4) * direction
            cls.vx = cls.vx * friction + cls.ax
            if(cls.vx > cls.speed): cls.vx = cls.speed
            if(cls.vx < -cls.speed): cls.vx = -cls.speed


        #Reaction en cas d'attaque
        if(player.action != False):
            ability = player.ability(player.action)
            range = (abs(player.x - cls.x) < ability.range)
            facing = ((player.sprite.facing == 0) & (player.x - cls.x <= 70)) | ((player.sprite.facing == 1) & (player.x - cls.x >= -70))
            if(range & facing):
                direction = 1
                if(player.x - cls.x > 0): direction = -1
                else: direction = 1
                cls.knocked = pygame.time.get_ticks()
                cls.ax = ability.knock * direction * 1.5
                cls.ay = -ability.knock * 2
                cls.game.sm.playHit()
                critical = player.critical()
                dmg = ability.base_damage + player.bonusDamage + randint(0, ability.base_damage) + critical * (ability.base_damage + player.bonusDamage)
                cls.hp = cls.hp - dmg
                cls.game.text.append(FloatingText(cls.display, int(dmg), cls.x - cls.cam.x, cls.y - 20, (critical>0)))
                if(cls.hp <= 0):
                    return False

        if((cls.knocked != -1) & (cls.knocked < pygame.time.get_ticks() - 500)):
            cls.knocked = -1

        if(cls.knocked != -1): cls.vx = cls.vx * friction + cls.ax
        cls.x += cls.vx
