from src.player import Player

class Camera:
    def __init__(cls, w, h, display):
        cls.w = w
        cls.h = h
        cls.x, cls.y = 100, 0
        cls.display = display
    def update(cls, player):
        cls.x = player.x - cls.w/4
