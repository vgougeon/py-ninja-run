import pygame

class Spell:
    def __init__(cls, spell, player):
        cls.name = spell["name"]
        cls.base_damage = spell["base_damage"]
        cls.cooldown = spell["cooldown"]
        cls.lastUse = 0
        cls.player = player
        cls.range = 160
        cls.knock = 5
    def use(cls):
        tick = pygame.time.get_ticks()
        if(tick > cls.lastUse + cls.cooldown):
            cls.lastUse = tick
            return cls.name
        else:
            return False
